from django.db import models
from datetime import datetime
from dateutil.parser import parse
import pandas as pd
# Create your models here.

class Horoskop:

    userHoroskop = {'Kobieta': {
        'Baran': "baran Opis Kobieta",
        'Byk': "Byk Opis Kobieta",
        'Bliźnięta': "Bliźnięta Opis Kobieta",
        'Rak': "Rak Opis Kobieta",
        'Lew': "Lew Opis Kobieta",
        'Panna': "Panna Opis Kobieta",
        'Waga': "Waga Opis Kobieta",
        'Skorpion': "Skorpion Opis Kobieta",
        'Strzelec': "Strzelec Opis Kobieta",
        'Koziorożec': "Koziorożec Opis Kobieta",
        'Wodnik': "Wodnik Opis Kobieta",
        'Ryby': "Ryby Opis Kobieta",
    }, 'Mężczyzna': {
        'Baran': "baran Opis Mężczyzna",
        'Byk': "Byk Opis Mężczyzna",
        'Bliźnięta': "Bliźnięta Opis Mężczyzna",
        'Rak': "Rak Opis Mężczyzna",
        'Lew': "Lew Opis Mężczyzna",
        'Panna': "Panna Opis Mężczyzna",
        'Waga': "Waga Opis Mężczyzna",
        'Skorpion': "Skorpion Opis Mężczyzna",
        'Strzelec': "Strzelec Opis Mężczyzna",
        'Koziorożec': "Koziorożec Opis Mężczyzna",
        'Wodnik': "Wodnik Opis Mężczyzna",
        'Ryby': "Ryby Opis Mężczyzna",
    }}

    partnerHoroskop = {'Kobieta': {
        'Baran': "baran Opis Kobieta",
        'Byk': "Byk Opis Kobieta",
        'Bliźnięta': "Bliźnięta Opis Kobieta",
        'Rak': "Rak Opis Kobieta",
        'Lew': "Lew Opis Kobieta",
        'Panna': "Panna Opis Kobieta",
        'Waga': "Waga Opis Kobieta",
        'Skorpion': "Skorpion Opis Kobieta",
        'Strzelec': "Strzelec Opis Kobieta",
        'Koziorożec': "Koziorożec Opis Kobieta",
        'Wodnik': "Wodnik Opis Kobieta",
        'Ryby': "Ryby Opis Kobieta",
    }, 'Mężczyzna': {
        'Baran': "baran Opis Mężczyzna",
        'Byk': "Byk Opis Mężczyzna",
        'Bliźnięta': "Bliźnięta Opis Mężczyzna",
        'Rak': "Rak Opis Mężczyzna",
        'Lew': "Lew Opis Mężczyzna",
        'Panna': "Panna Opis Mężczyzna",
        'Waga': "Waga Opis Mężczyzna",
        'Skorpion': "Skorpion Opis Mężczyzna",
        'Strzelec': "Strzelec Opis Mężczyzna",
        'Koziorożec': "Koziorożec Opis Mężczyzna",
        'Wodnik': "Wodnik Opis Mężczyzna",
        'Ryby': "Ryby Opis Mężczyzna",
    }}

    coupleHorskop = {
        'BaranBaran': "BaranBaran Opis para",
        'BaranByk': "BaranByk Opis para",
        'BaranBliźnięta': "BaranBliźnięta Opis para",
        'BaranRak': "BaranRak Opis para",
        'BaranLew': "BaranLewOpis para",
        'BaranPanna': "BaranPanna Opis para",
        'BaranWaga': "BaranWaga Opis para",
        'BaranSkorpion': "BaranSkorpion Opis para",
        'BaranStrzelec': "BaranBaran Opis para",
        'BaranKoziorożec': "BaranKoziorożec Opis para",
        'BaranWodnik': "BaranWodnik Opis para",
        'BaranRyby': "BaranRyby' Opis para",

        'BykBaran': "BykBaran Opis para",
        'BykByk': "BykByk Opis para",
        'BykBliźnięta': "BykBliźnięta Opis para",
        'BykRak': "BykRak Opis para",
        'BykLew': "BykLew Opis para",
        'BykPanna': "BykPanna Opis para",
        'BykWaga': "BykWaga Opis para",
        'BykSkorpion': "BykSkorpion Opis para",
        'BykStrzelec': "BykStrzelec Opis para",
        'BykKoziorożec': "BykKoziorożecOpis para",
        'BykWodnik': "BykWodnik Opis para",
        'BykRyby': "BykRyby Opis para",

        'BliźniętaBaran': "BliźniętaBaran Opis para",
        'BliźniętaByk': "BliźniętaByk Opis para",
        'BliźniętaBliźnięta': "BliźniętaBliźnięta Opis para",
        'BliźniętaRak': "BliźniętaRak Opis para",
        'BliźniętaLew': "BliźniętaLew Opis para",
        'BliźniętaPanna': "BliźniętaPanna Opis para",
        'BliźniętaWaga': "BliźniętaWaga Opis para",
        'BliźniętaSkorpion': "BliźniętaSkorpion Opis para",
        'BliźniętaStrzelec': "BliźniętaStrzelec Opis para",
        'BliźniętaKoziorożec': "BliźniętaKoziorożec Opis para",
        'BliźniętaWodnik': "BliźniętaWodnik Opis para",
        'BliźniętaRyby': "BliźniętaRyby Opis para",

        'RakBaran': "RakBaran Opis para",
        'RakByk': "RakByk Opis para",
        'RakBliźnięta': "RakBliźnięta Opis para",
        'RakRak': "RakRak Opis para",
        'RakLew': "RakLew Opis para",
        'RakPanna': "RakPanna Opis para",
        'RakWaga': "RakWaga Opis para",
        'RakSkorpion': "RakSkorpion Opis para",
        'RakStrzelec': "RakStrzelec Opis para",
        'RakKoziorożec': "RakKoziorożec Opis para",
        'RakWodnik': "RakWodnik Opis para",
        'RakRyby': "RakRyby Opis para",
    }


    def zodiac_sign(self, day, month):

        # checks month and date within the valid range
        # of a specified zodiac

        if month == 12:
            astro_sign = 'Strzelec' if (day < 22) else 'Koziorożec'

        elif month == 1:
            astro_sign = 'Koziorożec' if (day < 20) else 'Wodnik'

        elif month == 2:
            astro_sign = 'Wodnik' if (day < 19) else 'Ryby'

        elif month == 3:
            astro_sign = 'Ryby' if (day < 21) else 'Baran'

        elif month == 4:
            astro_sign = 'Baran' if (day < 20) else 'Byk'

        elif month == 5:
            astro_sign = 'Byk' if (day < 21) else 'Bliźnięta'

        elif month == 6:
            astro_sign = 'Bliźnięta' if (day < 21) else 'Rak'

        elif month == 7:
            astro_sign = 'Rak' if (day < 23) else 'Lew'

        elif month == 8:
            astro_sign = 'Lew' if (day < 23) else 'Panna'

        elif month == 9:
            astro_sign = 'Panna' if (day < 23) else 'Waga'

        elif month == 10:
            astro_sign = 'Waga' if (day < 23) else 'Skorpion'

        elif month == 11:
            astro_sign = 'Skorpion' if (day < 22) else 'Strzelec'

        return astro_sign

    def convert_string_to_date(self, birthDayString):
        return datetime.strptime(birthDayString, '%Y-%m-%d')

    def getUserHoroskop(self, zodiac, sex):
        return self.userHoroskop[sex][zodiac]

    def getPartnerHoroskop(self, zodiac, sex):
        return self.partnerHoroskop[sex][zodiac]

    def getCoupleHoroskop(self, zodiacUser, zodiacPartner):
        return self.coupleHorskop[zodiacUser+zodiacPartner]

    def getSexPartner(self, sexUser):
        if sexUser == "Kobieta":
            return "Mężczyzna"
        else:
            return "Kobieta"
