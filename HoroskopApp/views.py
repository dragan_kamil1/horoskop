from django.shortcuts import render


# Create your views here.
from HoroskopApp.models import Horoskop


def home(request):
    return render(request, 'home.html')


def result(request):

    horoskop = Horoskop()
    birthdayUser = request.POST.get('dateUser')
    birthdayPartner = request.POST.get('datePartner')
    birthdayUserDate = horoskop.convert_string_to_date(birthdayUser)
    birthdayPartnerDate = horoskop.convert_string_to_date(birthdayPartner)
    sexUser = request.POST.get('sex')
    sexPartner = horoskop.getSexPartner(sexUser)

    zodiacSignUser = horoskop.zodiac_sign(birthdayUserDate.day, birthdayUserDate.month)
    zodiacSignPartner = horoskop.zodiac_sign(birthdayPartnerDate.day, birthdayPartnerDate.month)

    userDescription = horoskop.getUserHoroskop(zodiacSignUser, sexUser)
    partnerDescription = horoskop.getPartnerHoroskop(zodiacSignPartner, sexPartner)
    coupleDescription = horoskop.getCoupleHoroskop(zodiacSignUser, zodiacSignPartner)

    context = {'birthdayUser': birthdayUser, 'birthdayPartner': birthdayPartner, "coupleDescription": coupleDescription,
               "userDescription": userDescription, "partnerDescription": partnerDescription}
    return render(request, 'result.html', context)
