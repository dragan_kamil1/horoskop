from django.apps import AppConfig


class HoroskopappConfig(AppConfig):
    name = 'HoroskopApp'
